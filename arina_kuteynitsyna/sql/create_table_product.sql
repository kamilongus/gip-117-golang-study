CREATE TABLE product
(
  id    SERIAL primary key,
  name  varchar(100) check (name <> '') NOT NULL ,
  price real check (price > 0)          NOT NULL ,
  type  varchar(30) check (type <> '')  NOT NULL
);
