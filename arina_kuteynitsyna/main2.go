package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Users struct {
	Users []User `json:"users"`
}
type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}
type User struct {
	Name string `json:"name"`
	Type string `json:"string"`
	Age  int    `json:"age"`
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	bytes, err := ioutil.ReadFile("example.json")
	checkError(err)
	var usersStruct Users
	err = json.Unmarshal(bytes, &usersStruct)
	checkError(err)
	for _, user := range usersStruct.Users {
		fmt.Println(user.Name)
		fmt.Println(user.Age)
		fmt.Println("______")
	}

}
