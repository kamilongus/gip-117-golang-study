package _

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type SpaceX struct {
	FlightNumber          int    `json:"flight_number"`
	MissionName           string `json:"mission_name"`
	Upcoming              bool   `json:"upcoming"`
	LaunchYear            string `json:"launch_year"`
	LaunchDateUnix        int    `json:"launch_date_unix"`
	LaunchDateUtc         string `json:"launch_date_utc"`
	LaunchDateLocal       string `json:"launch_date_local"`
	IsTentative           bool   `json:"is_tentative"`
	TentativeMaxPrecision string `json:"tentative_max_precision"`
	Tbd                   bool   `json:"tbd"`
	LaunchWindow          int    `json:"launch_window"`
	Rocket                Rocket `json:"rocket"`
}

type Rocket struct {
	RocketID    string      `json:"rocket_id"`
	RocketName  string      `json:"rocket_name"`
	RocketType  string      `json:"rocket_type"`
	FirstStage  FirstStage  `json:"first_stage"`
	SecondStage SecondStage `json:"second_stage"`
}

type FirstStage struct {
	Cores []Cores `json:"cores"`
}

type Cores struct {
	CoreSerial     string `json:"core_serial"`
	Flight         int    `json:"flight"`
	Block          int    `json:"block"`
	Legs           bool   `json:"legs"`
	Reused         bool   `json:"reused"`
	LandSuccess    bool   `json:"land_success"`
	LandingIntent  bool   `json:"landing_intent"`
	LandingType    string `json:"landing_type"`
	LandingVehicle string `json:"landing_vehicle"`
}

type SecondStage struct {
	Block    int        `json:"block"`
	Payloads []Payloads `json:"payloads"`
}

type Payloads struct {
	PayloadID string `json:"payload_id"`
	Reused    bool   `json:"reused"`
}

func HomeRouterHandler(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "My first API v1")
	checkError(err)

}

func listRouterHandler(w http.ResponseWriter, r *http.Request) {
	response, err := json.Marshal(GetSpaceXData())
	checkError(err)
	_, err = w.Write(response)
	checkError(err)
}

func firstElementHandler(w http.ResponseWriter, r *http.Request) {
	response, err := json.Marshal(GetSpaceXData()[0])
	checkError(err)
	_, err = w.Write(response)
	checkError(err)
}

func latestElementHandler(w http.ResponseWriter, r *http.Request) {
	spaceXList := GetSpaceXData()
	response, err := json.Marshal(spaceXList[len(spaceXList)-1])
	checkError(err)
	_, err = w.Write(response)
	checkError(err)
}

func main() {
	http.HandleFunc("/", HomeRouterHandler)
	http.HandleFunc("/list", listRouterHandler)
	http.HandleFunc("/list/first", firstElementHandler)
	http.HandleFunc("/list/latest", latestElementHandler)
	err := http.ListenAndServe(":9000", nil)
	checkError(err)
}

func GetSpaceXData() []SpaceX {

	resp, err := http.Get("https://api.spacexdata.com/v3/launches/")
	checkError(err)
	bytes, err := ioutil.ReadAll(resp.Body)
	checkError(err)
	var spaceXList []SpaceX

	err = json.Unmarshal(bytes, &spaceXList)
	checkError(err)
	return spaceXList
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
