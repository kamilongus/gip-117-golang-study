CREATE TABLE product
(
  id    SERIAL PRIMARY KEY                NOT NULL,
  name  VARCHAR(100) CHECK ( name <> '' ) NOT NULL,
  price REAL CHECK ( price >= 0 )         NOT NULL,
  type  VARCHAR(100) CHECK (type <> '' )  NOT NULL
);

INSERT INTO Product(name, price, type)
VALUES ('CS', 300, 'COMMON'),
       ('WOW', 500, 'SUB');