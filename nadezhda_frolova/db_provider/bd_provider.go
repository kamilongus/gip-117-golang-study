package db_provider

import (
	"../utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"io/ioutil"
	"net/http"
)

type Product struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Price       int    `json:"price"`
	ProductType string `json:"type"`
}

type Server struct {
	db *sql.DB
}

func (s *Server) ProductsHandler(w http.ResponseWriter, r *http.Request) {
	rows, err := s.db.Query("SELECT * FROM product")
	utils.CheckError(err)
	defer rows.Close()

	var products []Product

	for rows.Next() {
		product := Product{}

		err := rows.Scan(
			&product.Id,
			&product.Name,
			&product.Price,
			&product.ProductType)

		utils.CheckError(err)

		products = append(products, product)
	}
	bytes, err := json.Marshal(products)
	utils.CheckError(err)
	_, _ = w.Write(bytes)
}

func (s *Server) ProductByIdHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	product := Product{}

	rows, err := s.db.Query("SELECT * FROM product WHERE id = $1", id)
	utils.CheckError(err)
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(
			&product.Id,
			&product.Name,
			&product.Price,
			&product.ProductType)

		utils.CheckError(err)
	}
	bytes, err := json.Marshal(product)
	utils.CheckError(err)
	_, _ = w.Write(bytes)
}

func (s *Server) ProductUpdateHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	product := Product{}

	body, err := ioutil.ReadAll(r.Body)

	utils.CheckError(err)
	err = json.Unmarshal([]byte(body), &product)
	utils.CheckError(err)

	rows, err := s.db.Exec("UPDATE product SET name = $1, price = $2, type = $3 WHERE id = $4",
		product.Name, product.Price, product.ProductType, id)

	utils.CheckError(err)
	rows.RowsAffected()
}

func (s *Server) ProductDeleteHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	rowsPurchase, errPurchase := s.db.Query("DELETE FROM purchase WHERE product_id = $1", id)
	rows, err := s.db.Exec("DELETE FROM product WHERE id = $1", id)

	utils.CheckError(err)
	utils.CheckError(errPurchase)

	rows.RowsAffected()
	defer rowsPurchase.Close()
}

func (s *Server) ProductAddHandler(w http.ResponseWriter, r *http.Request) {
	product := Product{}

	body, err := ioutil.ReadAll(r.Body)

	utils.CheckError(err)
	err = json.Unmarshal([]byte(body), &product)
	utils.CheckError(err)
	fmt.Print(string(body))
	rows, err := s.db.Exec("INSERT INTO product (name, price, type) VALUES ($1, $2, $3)",
		product.Name, product.Price, product.ProductType)

	utils.CheckError(err)
	rows.RowsAffected()
}
