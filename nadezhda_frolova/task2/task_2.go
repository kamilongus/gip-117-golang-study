package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Example struct {
	Users []User `json:"users"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"age"`
	Social Social `json:"social"`
}

type Social struct {
	Twitter  string `json:"twitter"`
	Facebook string `json:"facebook"`
}

func checkError(er error) {
	if er != nil {
		panic(er)
	}
}

func main() {
	file, er := ioutil.ReadFile("example.json")
	checkError(er)

	u := Example{}
	er = json.Unmarshal(file, &u)
	checkError(er)
	for _, user := range u.Users {
		fmt.Println("Name: ", user.Name)
		fmt.Println("Type: ", user.Type)
		fmt.Println("Age: ", user.Age)
		fmt.Println("Social:")
		fmt.Println("		Facebook: ", user.Social.Facebook)
		fmt.Println("		Twitter: ", user.Social.Twitter)
	}
}
