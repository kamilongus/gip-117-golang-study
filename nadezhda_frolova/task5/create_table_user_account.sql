CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE user_account
(
  id          serial primary key,
  firstname   varchar(30) CHECK (firstname<>lastname ) NOT NULL ,
  lastname    varchar(60) CHECK (lastname<>firstname ) NOT NULL ,
  age         INT CHECK (age >= 0 and age <= 150) NOT NULL,
  external_id UUID DEFAULT uuid_generate_v4() NOT NULL
);