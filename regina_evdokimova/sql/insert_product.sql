INSERT INTO product (name, price, type)
VALUES ('highlighter', 900, 'SUB'),
       ('mascara', 500, 'COMMON'),
       ('concealer', 800, 'SUB'),
       ('lipstick', 600, 'SUB'),
       ('eyebrow pencil', 300, 'SUB'),
       ('eyeliner', 400, 'SUB');