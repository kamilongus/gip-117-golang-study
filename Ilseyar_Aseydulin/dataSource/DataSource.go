package dataSource

import (
	"../db_provider"
	"../utils"
	"database/sql"
)

func DataSource() struct{ db_provider.Server } {
	dataSource := "user=postgres password=rute dbname=mydb sslmode=disable"
	db, err := sql.Open("postgres", dataSource)
	C := db_provider.Server{db}
	utils.CheckError(err)
	return db
}
