SELECT u.*
FROM user_account AS u
       JOIN Purchase AS p_r ON p_r.user_id = u.id
       JOIN Product AS p ON p_r.product_id = p.id AND p.type = 'SUB';