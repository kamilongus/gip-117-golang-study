package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type Users struct {
	Users []User `json:"users"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"Age"`
	Social Social `json:"social"`
}

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

func PP(w http.ResponseWriter, r *http.Request) {
	var data []byte
	data, _ = ioutil.ReadFile("example.json")

	var str Users
	_ = json.Unmarshal(data, &str)

	arr := Users{}
	errors := json.Unmarshal(data, &arr)

	if errors != nil {
		log.Printf("Unmarshaled: %v", errors)
	}

	a, err := json.Marshal(arr.Users[0])
	f, err := json.Marshal(arr.Users[1])

	if err != nil {
		panic(err)
	}

	if r.URL.Path[1:] == "User_num_1" {
		w.Write(a)
	} else if r.URL.Path[1:] == "User_num_2" {
		w.Write(f)
	} else {
		w.WriteHeader(404)
	}

}

func main() {
	http.HandleFunc("/", PP)
	log.Fatal(http.ListenAndServe(":9000", nil))
}
