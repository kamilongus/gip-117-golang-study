package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
)

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"age"`
	Social Social `json:"social"`
}

type example struct {
	Users []User `json:"users"`
}

func main() {
	var data []byte
	data, _ = ioutil.ReadFile("example.json")

	var str example
	_ = json.Unmarshal(data, &str)

	arr := example{}
	error := json.Unmarshal(data, &arr)
	for _, user := range arr.Users {
		fmt.Println("Name:", user.Name)
		fmt.Println("	type:", user.Type)
		fmt.Println("	age:", user.Age)
		fmt.Println("	social:")
		fmt.Println("		facebook:", user.Social.Facebook)
		fmt.Println("		twitter:", user.Social.Twitter)
	}
	//tom := example {Name: "Tom"}
	//fmt.Println(tom.Name)
	if error != nil {
		log.Printf("Unmarshaled: %v", error)
	}

}
