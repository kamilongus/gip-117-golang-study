package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type UsersModel struct {
	Users []User `json:"users"`
}

type User struct {
	Name   string  `json:"name"`
	Type   string  `json:"type"`
	Age    int     `json:"age"`
	Social *Social `json:"social"`
}

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

func (u *UsersModel) print() {
	fmt.Println("users:")
	for count := range u.Users {
		fmt.Println("	name:", u.Users[count].Name)
		fmt.Println("	type:", u.Users[count].Type)
		fmt.Println("	age:", u.Users[count].Age)
		fmt.Println("	social:")
		fmt.Println("		facebook:", u.Users[count].Social.Facebook)
		fmt.Println("		twitter:", u.Users[count].Social.Twitter)
	}
}

func main() {

	file, errorFile := ioutil.ReadFile("example.json")

	if errorFile != nil {
		fmt.Println(errorFile)
	} else {
		var r = UsersModel{}
		errorJson := json.Unmarshal([]byte(file), &r)

		if errorJson != nil {
			fmt.Println(errorJson)
		} else {
			r.print()
		}
	}
}
