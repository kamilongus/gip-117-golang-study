CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE user_account
(
  id          SERIAL PRIMARY KEY,
  firstname   VARCHAR(30) CHECK ( lastname <> firstname ) NOT NULL,
  lastname    VARCHAR(60) CHECK ( firstname <> lastname ) NOT NULL,
  age         INT CHECK ( age >= 0 AND age < 150)         NOT NULL,
  external_id uuid                                        not null default uuid_generate_v4()
);