package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type Example struct {
	Users []User `json:"users"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"age"`
	Social Social `json:"social"`
}

type Social struct {
	Twitter  string `json:"twitter"`
	Facebook string `json:"facebook"`
}

func CheckError(er error) {
	if er != nil {
		panic(er)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	file, er := ioutil.ReadFile("example.json")
	CheckError(er)

	u := Example{}
	er = json.Unmarshal(file, &u)
	CheckError(er)

	bytes, err := json.Marshal(u.Users)
	_, err = w.Write(bytes)
	CheckError(err)

}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
