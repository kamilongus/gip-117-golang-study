package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type Team_info struct {
	Team_id         int     `json:"team_id"`
	Rating          float32 `json:"rating"`
	Wins            int     `json:"wins"`
	Losses          int     `json:"losses"`
	Last_match_time int     `json:"last_match_time"`
	Name            string  `json:"name"`
	Tag             string  `json:"tag"`
	Logo_url        string  `json:"logo_url"`
}

type Dota_info struct {
	Teams []Team_info
}

func home(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "My first API v1")
	checkError(err)
}

func list(w http.ResponseWriter, r *http.Request) {
	response, err := json.Marshal(Get_info())
	checkError(err)
	_, err = w.Write(response)
	checkError(err)
}

func firstElement(w http.ResponseWriter, r *http.Request) {
	response, err := json.Marshal(Get_info()[0])
	checkError(err)
	_, err = w.Write(response)
	checkError(err)
}

func latestElement(w http.ResponseWriter, r *http.Request) {
	response, err := json.Marshal(Get_info()[len(Get_info())-1])
	checkError(err)
	_, err = w.Write(response)
	checkError(err)
}

func main() {
	http.HandleFunc("/", home)
	http.HandleFunc("/list", list)
	http.HandleFunc("/list/first", firstElement)
	http.HandleFunc("/list/latest", latestElement)
	err := http.ListenAndServe(":8080", nil)
	checkError(err)
}

func Get_info() []Team_info {
	data, err := http.Get("https://api.opendota.com/api/teams/")
	checkError(err)
	bytes, err := ioutil.ReadAll(data.Body)
	checkError(err)
	var arr []Team_info
	err = json.Unmarshal(bytes, &arr)
	checkError(err)
	return arr
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
