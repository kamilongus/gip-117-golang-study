SELECT u.*, prod.*
FROM user_account AS u
  JOIN purchase AS purch ON purch.user_id = u.id
  JOIN product AS prod ON purch.product_id = prod.id AND prod.type = 'SUB';
