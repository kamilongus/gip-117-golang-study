package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"age"`
	Social Social `json:"social"`
}

type example struct {
	Users []User `json:"users"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	var data []byte
	data, err := ioutil.ReadFile("example.json")
	checkError(err)
	arr := example{}
	err = json.Unmarshal(data, &arr)
	checkError(err)
	b, err := json.Marshal(arr.Users[0])
	d, err := json.Marshal(arr.Users[1])
	if err != nil {
		panic(err)
	}
	if r.URL.Path[1:] == "name1" {
		w.Write(b)
	} else if r.URL.Path[1:] == "name2" {
		w.Write(d)
	} else {
		w.WriteHeader(404)
	}
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
