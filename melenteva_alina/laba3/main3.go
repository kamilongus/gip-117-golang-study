package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type Users struct {
	Users []User `json:"users"`
}
type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}
type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"age"`
	Social Social `json:"social"`
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
func handler(w http.ResponseWriter, r *http.Request) {
	b := r.URL.Path[1:]
	bytes, err := ioutil.ReadFile("example.json")
	checkError(err)
	var usersStruct Users
	err = json.Unmarshal(bytes, &usersStruct)
	checkError(err)
	users := make([]User, 0)

	for _, user := range usersStruct.Users {
		if strings.ToLower(user.Name) == strings.ToLower(b) {
			users = append(users, user)
		}

	}
	bytes, err = json.Marshal(users)
	checkError(err)
	_, err = w.Write(bytes)
	checkError(err)
}
func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))

}
