package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type User struct {
	Name string `json:"name"`
	Type string `json:"type"`
	Age  int    `json:"age"`

	Social Social `json:"social"`
}

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

type JsonObject struct {
	Users []User `json:"users"`
}

func main() {
	bytes, err := ioutil.ReadFile("example.json")
	if err != nil {
		fmt.Println(err)
	} else {
		var r = JsonObject{}
		err = json.Unmarshal(bytes, &r)

		if err != nil {
			fmt.Println(err)
		} else {
			for _, user := range r.Users {
				fmt.Println(user)

			}
		}
	}
}
