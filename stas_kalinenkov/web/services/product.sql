CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE product
(
    id    SERIAL PRIMARY KEY                NOT NULL,
    name  VARCHAR(100) CHECK ( name <> '' ) NOT NULL,
    price REAL CHECK ( price >= 0 )         NOT NULL,
    dsc  VARCHAR(100) CHECK (dsc <> '' )    NOT NULL
);


INSERT INTO product(name,
                   price,
                   dsc)

VALUES ('Мышь', 590, 'SUB'),
       ('Клавиатура', 1230, 'SUB'),
       ('Наушники', 930, 'SUB'),
       ('Колонки', 3230, 'SUB');