CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


CREATE TABLE user_account
(
  id          SERIAL PRIMARY KEY,
  firstname   VARCHAR(20) CHECK ( lastname <> firstname ) NOT NULL,
  lastname    VARCHAR(40) CHECK ( firstname <> lastname ) NOT NULL,
  age         INT CHECK ( age >= 0 AND age < 150)         NOT NULL,
  external_id uuid default uuid_generate_v4()             NOT NULL
);
